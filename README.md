[![JitPack](https://jitpack.io/v/org.bitbucket.DantheCodingGui/ShapeableBottomNavigationView.svg)](https://jitpack.io/#org.bitbucket.DantheCodingGui/ShapeableBottomNavigationView)

# Material Shapeable BottomNavigationView

A small subclass of Material Component's BottomNavigationView to support shape theming.

### Setup

#### Step One

Add the JitPack repository to your project level `build.gradle`:

```groovy
allprojects {
 repositories {
    google()
    jcenter()
    maven { url "https://jitpack.io" }
 }
}
```

#### Step Two

Add both ShapeableBottomNavigationView and Material Components to your app `build.gradle`:

```groovy
dependencies {
    implementation 'org.bitbucket.DantheCodingGui:ShapeableBottomNavigationView:<latest-version>'
    implementation 'com.google.android.material:material:<material-version>'
}
```

The latest library version is included in the Jitpack badge above.

#### Step Three

Ensure your app theme is inheriting one of the `Theme.MaterialComponents.*` themes.

### Usage

The easiest way to integrate the widget is to override the default `bottomNavigationStyle`
in your app's theme, and point to one of the .mcx styles or a subclass of it.

`<item name="bottomNavigationStyle">@style/Widget.Mcx.ShapeableBottomNavigationView</item>`

Subclasses of all the existing Bottom Navigation styles have been included.

Alternatively you can set the style manually in your layouts as per usual.

#### Edge-to-Edge

If you are planning on implementing edge-to-edge, then there are some attributes to consider.

The default widget will add extra padding to the bottom of the view to account for navigation UI insets.

To change this behavior:

`isFloating` Set this to true if you want to replace the extra padding for margins instead

`ignoreSystemInsets` Set this to true if you want no adjustments due to system insets (Useful if the bar is not at the bottom of your layout)

#### Shape

Shape is set the standard way with `shapeAppearance` and `shapeAppearanceOverlay`
attributes, in line with the other shapeable material components

The default shape of the widget matches `shapeAppearanceMediumComponent` 
with `0dp` corners on the bottom. To override this simply set a `shapeApperanceOverlay` in your 
`.Mcx` style subclass and it will take effect.
