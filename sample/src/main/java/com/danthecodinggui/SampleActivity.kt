package com.danthecodinggui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import com.danthecodinggui.mcx.R
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.shape.TriangleEdgeTreatment
import kotlinx.android.synthetic.main.activity_main.*

class SampleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // As with other shapeable components, can set the shapeAppearanceModel programatically
        // w/ edge treatments too
        val newShapeAppearanceModel = ShapeAppearanceModel.builder()
            .setAllCornerSizes(20f)
            .setLeftEdge(TriangleEdgeTreatment(20f, true))
            .setTopEdge(TriangleEdgeTreatment(20f, true))
            .setRightEdge(TriangleEdgeTreatment(20f, true))
            .setBottomEdge(TriangleEdgeTreatment(20f, true))
            .build()

        bnvProgrammaticPrimary.shapeAppearanceModel = newShapeAppearanceModel

        val testBadge = bnvProgrammaticPrimary.getOrCreateBadge(R.id.miExplore)
        testBadge.isVisible = true
        testBadge.number = 99

        // Edge to edge activation, if enabled 'isFloating' needs to be true to prevent the default
        // widget from adding unnecessary padding
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }
}