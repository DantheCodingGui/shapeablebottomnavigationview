package com.danthecodinggui.mcx

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.annotation.StyleRes
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.ViewCompat
import com.danthecodinggui.mcx.lib.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.internal.ViewUtils
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.shape.Shapeable

/**
 * A simple extension of BottomNavigationView that can accept a shapeAppearance
 */
open class ShapeableBottomNavigationView: BottomNavigationView, Shapeable {

    private var shapeAppearanceModel: ShapeAppearanceModel
    private var materialBackground: MaterialShapeDrawable?
    private var isFloating: Boolean = false
    private var shouldIgnoreSystemInsets: Boolean = false

    private var materialBackgroundDrawable: Drawable? = null

    constructor(context: Context): this(context, null)
    constructor(context: Context, attrs: AttributeSet?): this(
        context, attrs,
        R.attr.bottomNavigationStyle
    )
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(
        context,
        attrs,
        defStyleAttr
    ) {

        var attrBackgroundTintCol: ColorStateList? = null
        @StyleRes var shapeAppearanceResId = 0
        @StyleRes var shapeAppearanceOverlayResId = 0

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.ShapeableBottomNavigationView,
            defStyleAttr, 0
        ).apply {
            try {
                materialBackgroundDrawable = getDrawable(R.styleable.ShapeableBottomNavigationView_android_background)
                attrBackgroundTintCol = getColorStateList(R.styleable.ShapeableBottomNavigationView_android_backgroundTint)
                shapeAppearanceResId = getResourceId(
                    R.styleable.ShapeableBottomNavigationView_shapeAppearance,
                    0
                )
                shapeAppearanceOverlayResId = getResourceId(
                    R.styleable.ShapeableBottomNavigationView_shapeAppearanceOverlay,
                    0
                )
                isFloating = getBoolean(R.styleable.ShapeableBottomNavigationView_isFloating, false)
                shouldIgnoreSystemInsets = getBoolean(
                    R.styleable.ShapeableBottomNavigationView_ignoreSystemInsets,
                    false
                )
            } finally {
                recycle()
            }
        }

        shapeAppearanceModel = ShapeAppearanceModel.builder(
            context,
            shapeAppearanceResId,
            shapeAppearanceOverlayResId
        )
            .build()

        materialBackground = setMaterialShapedBackground(materialBackgroundDrawable)

        if (materialBackgroundDrawable == null || materialBackgroundDrawable is ColorDrawable) {
            attrBackgroundTintCol?.let { DrawableCompat.setTintList(
                background.mutate(),
                attrBackgroundTintCol
            ) }
        }

        background = materialBackground
    }

    private fun setMaterialShapedBackground(attrBackground: Drawable?): MaterialShapeDrawable {
        val drawable = MaterialShapeDrawable(shapeAppearanceModel)
        drawable.fillColor = if (attrBackground is ColorDrawable)
            ColorStateList.valueOf(attrBackground.color)
        else
            ColorStateList.valueOf(Color.LTGRAY)

        drawable.initializeElevationOverlay(context)
        drawable.elevation = ViewCompat.getElevation(this)

        return drawable
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        if (shouldIgnoreSystemInsets)
            ViewCompat.setOnApplyWindowInsetsListener(this, null)
        else if (isFloating)
            applyMarginSystemInsets()
    }

    @SuppressLint("RestrictedApi")
    private fun applyMarginSystemInsets() {
        val params = layoutParams as? MarginLayoutParams ?: return

        val originalMarginLeft = params.leftMargin
        val originalMarginTop = params.topMargin
        val originalMarginRight = params.rightMargin
        val originalMarginBottom = params.bottomMargin

        ViewCompat.setOnApplyWindowInsetsListener(this) { v, insets ->
            layoutParams = params.apply {
                setMargins(
                    originalMarginLeft,
                    originalMarginTop,
                    originalMarginRight,
                    originalMarginBottom + insets.systemWindowInsetBottom
                )
            }
            insets
        }
        ViewUtils.requestApplyInsetsWhenAttached(this)
    }

    override fun setElevation(elevation: Float) {
        super.setElevation(elevation)
        materialBackground?.elevation = elevation
    }

    override fun getShapeAppearanceModel(): ShapeAppearanceModel = shapeAppearanceModel
    override fun setShapeAppearanceModel(shapeAppearanceModel: ShapeAppearanceModel) {
        this.shapeAppearanceModel = shapeAppearanceModel
        materialBackground?.shapeAppearanceModel = shapeAppearanceModel
    }
}